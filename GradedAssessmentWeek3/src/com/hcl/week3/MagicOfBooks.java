package com.hcl.week3;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MagicOfBooks {
	
        private Map<Integer, Book> books;

		MagicOfBooks() {
			this.books = new HashMap<>();
		}

		public boolean addBook(int id, Book book) {
			books.put(id, book);
			return true;
		}

		public boolean deleteBook(int id) {
			 return books.remove(id) !=null;
		}

		public void updateBook(int id, Book book) {
			books.put(id, book);
			
		}

		public void displayAllTheBooks() {
			System.out.println(books);
		}

		public void totalCountOfBooks() {
			System.out.println(books.size());
		}

		public void displayByGenre(String genre) {
			Collection<Book> books2 = books.values();
			books2.stream().filter(book -> book.getGenre().equalsIgnoreCase(genre)).forEach(System.out::println);
		}

		public void displayInOrder() {
			List<Book> books2 = new ArrayList<>();
			for (Map.Entry<Integer, Book> map : books.entrySet()) {
				books2.add(map.getValue());
			}

			Collections.sort(books2, (book1, book2) -> book1.getPrice() > book2.getPrice() ? 1 : -1);
			System.out.println(books2);
		}

		public void displayInDescOrder() {
			List<Book> books2 = new ArrayList<>();
			for (Map.Entry<Integer, Book> map : books.entrySet()) {
				books2.add(map.getValue());
			}
			Collections.sort(books2, (book1, book2) -> book1.getPrice() > book2.getPrice() ? -1 : 1);
			System.out.println(books2);
		}

		public void displayInSoldOrder() {
			List<Book> books2 = new ArrayList<>();
			for (Map.Entry<Integer, Book> map : books.entrySet()) {
				books2.add(map.getValue());
			}
			Collections.sort(books2, (book1, book2) -> book1.getNoOfCopyesSold() > book2.getNoOfCopyesSold() ? -1 : 1);
			System.out.println(books2);
		}

		public static void main(String[] args)  {
			MagicOfBooks magicOfBooks = new MagicOfBooks();
			magicOfBooks.addBook(1, new Book("Fifty Shades of Grey",100, "Drama", 4));
			magicOfBooks.addBook(2, new Book("The tortoise and the hare",50, "Fantasy", 6));
			magicOfBooks.addBook(3, new Book("A star is born", 150, "Drama", 9));
			magicOfBooks.addBook(4, new Book("the lord of the rings series", 150, "Adventure", 2));
			magicOfBooks.addBook(5, new Book("The Last Legion", 45, "Comedy", 1));
			magicOfBooks.addBook(6, new Book("A Woman in Black by Susan Hill", 300, "Horror", 3));
			
			
			magicOfBooks.displayAllTheBooks();
			System.out.println("display by genre");
		    magicOfBooks.displayByGenre("Drama");
			magicOfBooks.displayInDescOrder();
			magicOfBooks.displayInOrder();
			magicOfBooks.displayInSoldOrder();
			System.out.println("display all the books by deletebook :3");
			magicOfBooks.deleteBook(3);
			magicOfBooks.displayAllTheBooks();
			magicOfBooks.updateBook(3, new Book("Postmortem",35,"Drama",5));
			magicOfBooks.displayAllTheBooks();
			magicOfBooks.totalCountOfBooks();
			System.out.println("display all the books by deletebook :5");
			magicOfBooks.deleteBook(5);
			magicOfBooks.displayAllTheBooks();
			
			
			}
		
          }

			
		
			
			
		



