package com.hcl.week3;

public class Book {  // Create book as a pojo class.
	
		private String name;  // add Name ,Price,Genre,noOfCopyesSold attributes
		private double price;
		private String genre;
		private int noOfCopyesSold;
		public Book() {
			super();
		}
		public Book(String name, double price, String genre, int noOfCopyesSold) {
			super();
			this.name = name;
			this.price = price;
			this.genre = genre;
			this.noOfCopyesSold = noOfCopyesSold;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public double getPrice() {
			return price;
		}
		public void setPrice(double price) {
			this.price = price;
		}
		public String getGenre() {
			return genre;
		}
		public void setGenre(String genre) {
			this.genre = genre;
		}
		public int getNoOfCopyesSold() {
			return noOfCopyesSold;
		}
		public void setNoOfCopyesSold(int noOfCopyesSold) {
			this.noOfCopyesSold = noOfCopyesSold;
		}
		@Override
		public String toString() {
			return "Book [name=" + name +"\n" +" price=" + price +"\n "+" genre=" + genre +"\n "+ "noOfCopyesSold=" + noOfCopyesSold
					+ "]";
		}
		
}		
	


